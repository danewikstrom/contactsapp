//
//  ContactViewController.swift
//  ContactsApp
//
//  Created by Dane Wikstrom on 10/14/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI
import MapKit

class ContactViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate {
    
    

    @IBOutlet weak var nameTextField: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneNumTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var streetAddressTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    
    
    let realm = try! Realm()
    var contact: Contact!
    var updatedContact: Contact!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        nameTextField.text = contact.fName + " " + contact.lName
        firstNameTextField.text = contact.fName
        lastNameTextField.text = contact.lName
        phoneNumTextField.text = contact.phoneNum
        emailTextField.text = contact.email
        streetAddressTextField.text = contact.street
        cityTextField.text = contact.city
        stateTextField.text = contact.state
        postalCodeTextField.text = contact.postalCode
        
        let button = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.edit, target: self, action: #selector(ContactViewController.editButtonTapped))
        navigationItem.rightBarButtonItem = button
        
        stateTextField.delegate = self
        
        let location = contact.street + ", " + contact.state + ", " + contact.postalCode
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location) { [weak self] placemarks, error in
            if let placemark = placemarks?.first, let location = placemark.location
            {
                let mark = MKPlacemark(placemark: placemark)
                
                if var region = self?.mapView.region
                {
                    region.center = location.coordinate
                    region.span.longitudeDelta /= 8.0
                    region.span.latitudeDelta /= 8.0
                    self?.mapView.setRegion(region, animated: true)
                    self?.mapView.addAnnotation(mark)
                }
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func editButtonTapped()
    {
        firstNameTextField.isEnabled = true
        lastNameTextField.isEnabled = true
        phoneNumTextField.isEnabled = true
        emailTextField.isEnabled = true
        streetAddressTextField.isEnabled = true
        cityTextField.isEnabled = true
        stateTextField.isEnabled = true
        postalCodeTextField.isEnabled = true
        firstNameTextField.clearButtonMode = .always
        lastNameTextField.clearButtonMode = .always
        phoneNumTextField.clearButtonMode = .always
        emailTextField.clearButtonMode = .always
        streetAddressTextField.clearButtonMode = .always
        cityTextField.clearButtonMode = .always
        stateTextField.clearButtonMode = .always
        postalCodeTextField.clearButtonMode = .always
        
        let button = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(ContactViewController.saveButtonTapped))
        navigationItem.rightBarButtonItem = button
        navigationItem.rightBarButtonItem?.tintColor = UIColor.red
        
    }
    
    func saveButtonTapped()
    {
        firstNameTextField.isEnabled = false
        lastNameTextField.isEnabled = false
        phoneNumTextField.isEnabled = false
        emailTextField.isEnabled = false
        streetAddressTextField.isEnabled = false
        cityTextField.isEnabled = false
        stateTextField.isEnabled = false
        postalCodeTextField.isEnabled = false
        firstNameTextField.clearButtonMode = .never
        lastNameTextField.clearButtonMode = .never
        phoneNumTextField.clearButtonMode = .never
        emailTextField.clearButtonMode = .never
        streetAddressTextField.clearButtonMode = .never
        cityTextField.clearButtonMode = .never
        stateTextField.clearButtonMode = .never
        postalCodeTextField.clearButtonMode = .never
        
        //Set edit button
        let button = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.edit, target: self, action: #selector(ContactViewController.editButtonTapped))
        navigationItem.rightBarButtonItem = button
    
        updatedContact = contact

        //Update with new record
        try! realm.write
        {
            updatedContact.fName = firstNameTextField.text!
            updatedContact.lName = lastNameTextField.text!
            updatedContact.phoneNum = phoneNumTextField.text!
            updatedContact.email = emailTextField.text!
            updatedContact.street = streetAddressTextField.text!
            updatedContact.city = cityTextField.text!
            updatedContact.postalCode = postalCodeTextField.text!
        }
        nameTextField.text = updatedContact.fName + " " + updatedContact.lName
        
        //Update map with updatedContacts new info
        let location = updatedContact.street + ", " + updatedContact.state + ", " + updatedContact.postalCode
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location) { [weak self] placemarks, error in
            if let placemark = placemarks?.first, let location = placemark.location
            {
                let mark = MKPlacemark(placemark: placemark)
                
                if var region = self?.mapView.region
                {
                    region.center = location.coordinate
                    region.span.longitudeDelta /= 8.0
                    region.span.latitudeDelta /= 8.0
                    self?.mapView.setRegion(region, animated: true)
                    self?.mapView.addAnnotation(mark)
                }
            }
        }
    }
    
    @IBAction func callButtonTapped(_ sender: UIButton)
    {
        //calling
        if let url = URL(string: "tel://\(contact.phoneNum as String)"), UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10, *)
            {
                UIApplication.shared.open(url)
            }
            else
            {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func smsButtonTapped(_ sender: UIButton)
    {
        //Send SMS
        if (MFMessageComposeViewController.canSendText())
        {
            let controller = MFMessageComposeViewController()
            //controller.body = "Message Body"
            controller.recipients = [contact.phoneNum as String]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
        else
        {
            print("SMS services are not available")
            return
        }
    }
    
    @IBAction func sendEmailButtonTapped(_ sender: UIButton)
    {
        //Send Email
        if (MFMailComposeViewController.canSendMail())
        {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.setToRecipients([contact.email as String])
            //composeVC.setSubject("Hello!")
            //composeVC.setMessageBody("Hello this is my message body!", isHTML: false)
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
        else
        {
            print("Mail services are not available")
            return
        }
    }
    
    // The mail compose view controller is not dismissed automatically. When the user taps the buttons to send the email or cancel the interface,
    // the mail compose view controller calls the mailComposeController(_:didFinishWith:error:) method of its delegate. Your implementation of
    // that method must dismiss the view controller explicitly
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //only let two chars be in state text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        guard let text = stateTextField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 2
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let vc = segue.destination as! ViewController
        vc.updatedContact = updatedContact
        vc.oldContact = contact
    }
}
