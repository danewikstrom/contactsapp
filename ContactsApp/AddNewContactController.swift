//
//  AddNewContactController.swift
//  ContactsApp
//
//  Created by Dane Wikstrom on 10/14/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit
import RealmSwift

class AddNewContactController: UIViewController, UITextFieldDelegate
{
    let realm = try! Realm()
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!
    

    @IBAction func addContactButtonTapped(_ sender: UIButton)
    {
        try! realm.write
        {
            let newContact = Contact()
            newContact.fName = firstNameTextField.text!
            newContact.lName = lastNameTextField.text!
            newContact.phoneNum = phoneNumberTextField.text!
            newContact.email = emailTextField.text!
            newContact.street = streetTextField.text!
            newContact.city = cityTextField.text!
            newContact.state = stateTextField.text!
            newContact.postalCode = zipTextField.text!
            
            self.realm.add(newContact)
        }
        performSegue(withIdentifier: "ContactNav", sender: self)
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton)
    {
        performSegue(withIdentifier: "ContactNav", sender: self)
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()

        stateTextField.delegate = self
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //only let two chars be in state text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        guard let text = stateTextField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 2
    }
}
