//
//  ViewController.swift
//  ContactsApp
//
//  Created by Dane Wikstrom on 10/12/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//
//https://realm.io/docs/swift/latest/
//https://www.raywenderlich.com/112544/realm-tutorial-getting-started
//Migrations - https://stackoverflow.com/questions/34891743/realm-migrations-in-swift
import UIKit
import RealmSwift
import MessageUI

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    lazy var contacts: Results<Contact> = { self.realm.objects(Contact.self) }()
    var selectedContact: Contact!//used to store the currently selected contact
    var updatedContact: Contact!
    var oldContact: Contact!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        populateDefaultContacts()
    }
    
    func populateDefaultContacts()
    {
        if contacts.count == 0
        {
            try! realm.write()
            {
                let defaultContact = Contact()
                defaultContact.fName = "Dane"
                defaultContact.lName = "Wikstrom"
                defaultContact.phoneNum = "8015408810"
                defaultContact.email = "danewikstrom@mail.weber.edu"
                defaultContact.street = "1624 21st Street"
                defaultContact.city = "Ogden"
                defaultContact.state = "UT"
                defaultContact.postalCode = "84401"
                
                self.realm.add(defaultContact)
            }
        }
        contacts = realm.objects(Contact.self)
        print(contacts)
    }

    //find the number of rows in the tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return contacts.count
    }
    
    //insert the contacts into the tableview
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Contact Cell", for: indexPath)
        let contact = contacts[indexPath.row]
        cell.textLabel?.text = contact.fName + " " + contact.lName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        selectedContact = contacts[indexPath.row]
        performSegue(withIdentifier: "Show Contact", sender: nil)
        tableView.deselectRow(at: indexPath, animated: true)//deselect row
    }
    
    //swift left to delete contact
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.delete
        {
            selectedContact = contacts[indexPath.row]
            try! realm.write
            {
                realm.delete(selectedContact)
            }
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }

    //Load tableview with correct data
    override func viewWillAppear(_ animated: Bool)
    {
        contacts = realm.objects(Contact.self)
        super.viewWillAppear(true)
        tableView.reloadData()
    }
    
    @IBAction func addButtonTapped(_ sender: UIBarButtonItem)
    {
        performSegue(withIdentifier: "AddNewContact", sender: self)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier != "AddNewContact"
        {
            let vc = segue.destination as! ContactViewController
            vc.contact = selectedContact
        }
    }
}

