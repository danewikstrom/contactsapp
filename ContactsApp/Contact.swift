//
//  Contact.swift
//  ContactsApp
//
//  Created by Dane Wikstrom on 10/12/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import Foundation
import RealmSwift

class Contact : Object
{
    dynamic var fName = ""
    dynamic var lName = ""
    dynamic var phoneNum = ""
    dynamic var street = ""
    dynamic var city = ""
    dynamic var state = ""
    dynamic var postalCode = ""
    dynamic var email = ""
}
